<?php

namespace C33s\Bundle\FormExtraBundle\DependencyInjection;

use InvalidArgumentException;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('c33s_form_extra');
        $rootNode = $this->getRootNode($builder);

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $builder;
    }

    /**
     * Symfony Config BC layer to allow legacy and new instance mode (was changed in Symfony 4.2).
     *
     * @see https://symfony.com/doc/4.2/components/config/definition.html#defining-a-hierarchy-of-configuration-values-using-the-treebuilder
     * @see https://github.com/dustin10/VichUploaderBundle/issues/999
     */
    private function getRootNode(TreeBuilder $builder): NodeDefinition
    {
        if (\method_exists($builder, 'getRootNode')) {
            return $builder->getRootNode();
        }
        if (\method_exists($builder, 'root')) {
            return $builder->root('c33s_form_extra');
        }

        throw new InvalidArgumentException('TreeBuilder must implement `getRootNode()` or `root()`');
    }
}
